﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE16A_Repetition_arrayer_metoder
{
    class Program
    {
        static void Main(string[] args)
        {

            string[] name = { "Kalle", "Berthild", "Kim", "Dennis" };

            int k = 2;

            List<string> longNames = new List<string>();

            /*int[] pos = { 2, 1,0 };

            Console.WriteLine(name[pos[pos[2]]]);*/

            name[1] = "Brynhilde";

            for (int i = 0; i < name.Length; i++)
            {
                if (name[i].Length > 4)
                {
                    //Console.WriteLine(name[i]);
                    longNames.Add(name[i]);
                }
            }

            Console.WriteLine(longNames.Count);

            Hello("Herbert");

            Hellos(name);

            Console.ReadLine();
        }

        static void Hellos(string[] ns)
        {
            for (int i = 0; i < ns.Length; i++)
            {
                Hello(ns[i]);
            }
        }

        static void Hello(string n)
        {
            Console.WriteLine(n + " SAYS HELLO");

            Goodbye();

        }

        static void Goodbye()
        {
            Console.WriteLine("YOU SAY GOODBYE");
        }

    }
}
