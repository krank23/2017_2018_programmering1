﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public float speed = 2;
    public float jumpForce = 10;

    public Transform groundCheck;

    public LayerMask groundLayer;

    bool hasReleasedJump = true;

    bool isGrounded = false;

	// Use this for initialization
	void Start () {
		
	}

    void FixedUpdate()
    {
        if(Physics2D.OverlapCircle(groundCheck.position, 0.05f, groundLayer))
        {
            isGrounded = true;
        }
        else
        {
            isGrounded = false;
        }
    }

    // Update is called once per frame
    void Update () {

        float xMove = Input.GetAxisRaw("Horizontal");

        Vector2 movement = Vector2.right * xMove * speed * Time.deltaTime;

        transform.Translate(movement);

        // JUMP

        float jump = Input.GetAxisRaw("Jump");

        if (jump > 0 && hasReleasedJump == true && isGrounded == true)
        {
            hasReleasedJump = false;

            print("JUMP");

            Vector2 jumpForceVector = Vector2.up * jumpForce;

            Rigidbody2D rb = GetComponent<Rigidbody2D>();

            rb.AddForce(jumpForceVector);

        }
        else if (!(jump > 0)) {
            hasReleasedJump = true;
        }
	}
}
