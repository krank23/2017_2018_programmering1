﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public GameObject followObject;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        
        this.transform.position = Vector3.Lerp((Vector2) transform.position, (Vector2) followObject.transform.position, 0.1f);

	}
}
