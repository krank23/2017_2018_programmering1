﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickerController : MonoBehaviour {

    bool follow = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (follow == true)
        {
            Vector2 mouseScreen = Input.mousePosition;
            Vector2 mouseWorld = Camera.main.ScreenToWorldPoint(mouseScreen);
            //print(mouseWorld.x);

            GetComponent<Rigidbody2D>().gravityScale = 5;

            this.transform.position = Vector3.Lerp(transform.position, mouseWorld, 0.1f);
        }
	}

    void OnMouseUp()
    {
        follow = !follow;
    }

    void OnMouseOver()
    {
        //print("OVER!");
    }
}
