﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE15A_TypKonvertering
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Skriv ett tal du vill multiplicera med 9: ");

            int ti = -1;
            
            while (ti < 0)
            {
                string t = Console.ReadLine();
                if (t.All(Char.IsDigit))
                {
                    ti = int.Parse(t);
                }
            }
            
            
            Console.WriteLine( ti * 9 );

            Console.ReadLine();
        }
    }
}
