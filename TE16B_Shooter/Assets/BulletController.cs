﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        float speed = 5f;

        Vector3 movementY = Vector3.up * speed * Time.deltaTime;

        transform.Translate(movementY);

        if (Mathf.Abs(transform.position.y) > 5f)
        {
            Destroy(this.gameObject);
        }
        else if (Mathf.Abs(transform.position.x) > 3)
        {
            Destroy(this.gameObject);
        }
    }
}
