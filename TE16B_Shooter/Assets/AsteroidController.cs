﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidController : MonoBehaviour {

    [SerializeField]
    GameObject exploPrefab;

	// Use this for initialization
	void Start () {
       
        float x = Random.Range(-2f, 2f);

        Vector3 pos = new Vector3(x, 6);

        transform.position = pos;

	}
	
	// Update is called once per frame
	void Update () {
        float speed = 5f;

        Vector3 movementY = Vector3.down * speed * Time.deltaTime;

        transform.Translate(movementY);

        if (transform.position.y < -6f)
        {
            Destroy(this.gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Bullet")
        {
            KillItWithFire();
        }
    }

    public void KillItWithFire()
    {
        Destroy(this.gameObject);
        Instantiate(exploPrefab, transform.position, Quaternion.identity);

        GameObject playerShip = GameObject.FindGameObjectWithTag("Player");

        if (playerShip != null)
        {
            PlayerController pc = playerShip.GetComponent<PlayerController>();
            pc.AddPoints();
        }
    }

}
