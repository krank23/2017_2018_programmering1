﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidGenerator : MonoBehaviour {

    float timer = 0;
    float timerMax = 2f;

    [SerializeField]
    GameObject asteroidPrefab;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        timer += Time.deltaTime;
        
        if (timer > timerMax)
        {
            Instantiate(asteroidPrefab);
            timer = 0;

            timerMax *= 0.95f;

        }

	}
}
