﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{

    [SerializeField]
    GameObject bulletPrefab;

    [SerializeField]
    Text scoreText;

    int points = 0;

    float shootTimerMax = 0.25f;
    float shootTimer = 0;

    bool hasReleasedFire2 = true;

    // Use this for initialization
    void Start()
    {
        //AddPoints();
    }

    // Update is called once per frame
    void Update()
    {

        float speed = 3.5f;

        float moveX = Input.GetAxisRaw("Horizontal");
        float moveY = Input.GetAxisRaw("Vertical");

        Vector3 movementX = Vector3.right * speed * moveX * Time.deltaTime;
        Vector3 movementY = Vector3.up * speed * moveY * Time.deltaTime;

        transform.Translate(movementX + movementY);

        if (transform.position.x > 2.3f || transform.position.x < -2.3f)
        {
            transform.Translate(-movementX);
        }

        if (transform.position.y > 4.5f || transform.position.y < -4.5f)
        {
            transform.Translate(-movementY);
        }

        // Shoot code

        shootTimer += Time.deltaTime;

        if (Input.GetAxisRaw("Fire1") > 0 && shootTimer > shootTimerMax)
        {
            Instantiate(bulletPrefab, transform.position, Quaternion.identity);
            shootTimer = 0;
        }

        if (Input.GetAxisRaw("Fire2") > 0 && hasReleasedFire2)
        {
            OctoShot();
            hasReleasedFire2 = false;
        }

        if (!(Input.GetAxisRaw("Fire2") > 0))
        {
            hasReleasedFire2 = true;
        }

        if (Input.GetAxisRaw("Fire3") > 0)
        {
            BigBadaBoom();
        } 

    }

    void OctoShot()
    {
        for (int i = 0; i < 8; i++)
        {
            GameObject newBullet = Instantiate(bulletPrefab, transform.position, Quaternion.identity);

            newBullet.transform.Rotate(Vector3.back, i * 45);

        }
    }

    void BigBadaBoom()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");

        for (int i = 0; i < enemies.Length; i++)
        {
            //Destroy(enemies[i]);

            AsteroidController ac = enemies[i].GetComponent<AsteroidController>();
            ac.KillItWithFire();

        }

        //print(enemies.Length);

    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Enemy")
        {
            print("Ajjj!");
            SceneManager.LoadScene(1);
        }
    }

    public void AddPoints()
    {
        points++;
        scoreText.text = points.ToString();
    }

}
