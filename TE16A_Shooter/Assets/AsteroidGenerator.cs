﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidGenerator : MonoBehaviour {

    [SerializeField]
    GameObject asteroidPrefab;

    float asteroidTimer = 0;
    float asteroidTimerMax = 2f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        asteroidTimer += Time.deltaTime;

        if (asteroidTimer > asteroidTimerMax)
        {
            asteroidTimer = 0;
            Instantiate(asteroidPrefab);

            asteroidTimerMax *= 0.8f;

        }

        
	}
}
