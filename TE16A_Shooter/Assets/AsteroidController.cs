﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidController : MonoBehaviour {

    float speed = 6;

    [SerializeField]
    GameObject explosion;

    // Use this for initialization
    void Start () {

        float x = Random.Range(-2f, 2f);

        Vector3 newPosition = new Vector3(x, 6f);

        transform.position = newPosition;

	}
	
	// Update is called once per frame
	void Update () {

        Vector2 movementY = Vector2.down * speed * Time.deltaTime;

        transform.Translate(movementY);

        if (transform.position.y < -6f)
        {
            Destroy(this.gameObject);
        }

    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag != "Enemy")
        {
            KillItWithFire();
        }

    }

    public void KillItWithFire()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");

        PlayerController pc = player.GetComponent<PlayerController>();

        pc.AddPoints(1);

        Destroy(this.gameObject);
        Instantiate(explosion);
    }
}
