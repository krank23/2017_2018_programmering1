﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour {

    float speed = 6;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        Vector2 movementY = Vector2.up * speed * Time.deltaTime;

        transform.Translate(movementY);

        if (Mathf.Abs(transform.position.y) > 5f)
        {
            Destroy(this.gameObject);
        }


        if (Mathf.Abs(transform.position.x) > 3f)
        {
            Destroy(this.gameObject);
        }

    }


    void OnTriggerEnter2D(Collider2D collision)
    {
        Destroy(this.gameObject);
    }
}
