﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {

    int points = 0;

    [SerializeField]
    Text scoreText;

    [SerializeField]
    float speed;

    [SerializeField]
    GameObject bullet;

    float shootTimer = 0;
    float shootTimerMax = 0.5f;

    bool hasReleasedFire2 = true;

    public void AddPoints(int p)
    {
        points += p;

        scoreText.text = points.ToString();

    }

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {

        float moveX = Input.GetAxisRaw("Horizontal");
        float moveY = Input.GetAxisRaw("Vertical");

        Vector2 movementX = Vector2.right * speed * moveX * Time.deltaTime;
        Vector2 movementY = Vector2.up * speed * moveY * Time.deltaTime;

        transform.Translate(movementX + movementY);

        if (transform.position.x > 2.2f || transform.position.x < -2.2f)
        {
            transform.Translate(-movementX);
        }

        if (transform.position.y > 4.5f || transform.position.y < -4.5f)
        {
            transform.Translate(-movementY);
        }


        // Shooting

        shootTimer += Time.deltaTime;
        
        if (Input.GetAxisRaw("Fire1") > 0 && shootTimer > shootTimerMax)
        {
            shootTimer = 0;
            Instantiate(bullet, transform.position, Quaternion.identity);
        }
        
        if (Input.GetAxisRaw("Fire2") > 0 && hasReleasedFire2)
        {
            OctoShot();
            hasReleasedFire2 = false;
        }

        if (!(Input.GetAxisRaw("Fire2") > 0) )
        {
            hasReleasedFire2 = true;
        }

        if (Input.GetAxisRaw("Fire3") > 0)
        {
            BigBadaBoom();
        }

    }

    void OctoShot()
    {
        for (int i = 0; i < 8; i++)
        {
            GameObject newBullet = Instantiate(bullet, transform.position, Quaternion.identity);

            newBullet.transform.Rotate(Vector3.back, 45*i);

        }
    }

    void BigBadaBoom()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");

        for (int i = 0; i < enemies.Length; i++)
        {

            AsteroidController ac = enemies[i].GetComponent<AsteroidController>();

            ac.KillItWithFire();

            //Destroy(enemies[i]);
        }

    }



    void OnTriggerEnter2D(Collider2D collision)
    {
        SceneManager.LoadScene(1);

        //Destroy(this.gameObject);
    }
}
