﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE16B_StringManipulation
{
    class Program
    {
        static void Main(string[] args)
        {
            string name = "Micke";

            name = name.ToLower();

            Console.WriteLine(name);

            //if (name == "Micke" || name== "MICKE" || name == "micke")

            //if (name.ToLower() == "micke")

            // Gorgon;12;100;2

            string welcome = "  hej  ";

            Console.WriteLine(welcome.Trim() + name);

            string s = "Micke Martin Lena";

            string[] words = s.Split(' ');

            words = words.Reverse().ToArray();

            string newWord = String.Join(" är dum och ", words);

            Console.WriteLine(newWord);


            //Console.WriteLine(newWord.IndexOf("dum"));
            Console.WriteLine(newWord.Replace("dum","smart"));

            Console.WriteLine(newWord.Substring(2, 8));

            /* ToLower
             * ToUpper
             * Trim
             * Split
             * Join
             * IndexOf
             * Replace
             * Substring
             * 
             */

            Console.ReadLine();
        }
    }
}
