﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE16A_Repetition
{
    class Program
    {
        static void Main(string[] args)
        {
            /*int x = 87;

            x = 99;

            string name = Console.ReadLine();

            Console.WriteLine("hello " + name);*/

            string name = "";
            string password = "";

            while(name != "Micke" || password != "12345")
            {
                Console.WriteLine("Användarnamn:");
                name = Console.ReadLine();

                Console.WriteLine("Password:");
                password = Console.ReadLine();

                if (name == "Micke" && password == "12345")
                {
                    Console.WriteLine("Rätt!");
                }
                else
                {
                    Console.WriteLine("Fel, dumhuve!");
                }
            }

            Console.WriteLine("Du är nu inne i systemet. Woooh!");

            Console.ReadLine();

        }
    }
}
