﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    bool follow = false;

    Vector2 targetPos;

    void Start()
    {
        targetPos = transform.position;
    }

    // Update is called once per frame
    void Update () {

        transform.position = Vector3.Lerp(transform.position, targetPos, 0.05f);

        //if (follow == true)
        if (Input.GetMouseButton(0))
        {
            Vector2 mPosScreen = Input.mousePosition;

            Vector2 mPosWorld = Camera.main.ScreenToWorldPoint(mPosScreen);

            targetPos = mPosWorld;

            //transform.position = Vector3.Lerp(transform.position, mPosWorld, 0.05f);
        }
	}
    void OnMouseUp()
    {
        follow = !follow;
    }
}
