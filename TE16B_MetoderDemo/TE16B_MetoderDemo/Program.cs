﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE16B_MetoderDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 99;

            Hello(18, "Micke");
            //string grej = GetText();
            Loop(10);
            Console.ReadLine();
        }

        static void Loop(int t)
        {
            Console.WriteLine("WOOP!");
            if (t > 0)
            {
                Loop(t - 1);
            }
        }

        static string GetText()
        {
            string input = "";
            while (input.Length == 0)
            {
                input = Console.ReadLine();
                input = input.Trim();
            }

            return input;
        }
        static void Hello(int tal, string text)
        {
            int counter = 0;
            while (counter < tal)
            {
                Console.WriteLine("HELLO " + text);
                counter++;
            }

        }

    }
}
