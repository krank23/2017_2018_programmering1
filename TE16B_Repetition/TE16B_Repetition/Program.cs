﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE16B_Repetition
{
    class Program
    {
        static void Main(string[] args)
        {
            // Datatyper, variabler

            int iq = 87 % 2;

            // if-satser
            /*Console.WriteLine("Skriv in ett användarnamn: ");
            string username = Console.ReadLine();

            Console.WriteLine("Skriv in lösenordet: ");
            string password = Console.ReadLine();

            if (password == "12345" && username == "kalle")
            {
                Console.WriteLine("CONGRATS");
            }
            else
            {
                Console.WriteLine("FUllständigt fel");
            }*/

            // loopar

            string username = "";
            string password = "";

            while (password != "12345" || username != "kalle")
            {
                Console.WriteLine("Skriv in ett användarnamn: ");
                username = Console.ReadLine();

                Console.WriteLine("Skriv in lösenordet: ");
                password = Console.ReadLine();

                if (password == "12345" && username == "kalle")
                {
                    Console.WriteLine("CONGRATS");
                }
                else
                {
                    Console.WriteLine("FUllständigt fel");
                }
            }

            Console.ReadLine();
        }
    }
}
