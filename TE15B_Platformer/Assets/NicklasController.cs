﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NicklasController : MonoBehaviour {

    public float speed = 2;
    public float jumpForce = 100;

    public Transform groundCheck;

    public Transform topLeftGroundCheck;
    public Transform bottomRightGroundCheck;

    public LayerMask groundLayer;

    bool isGrounded = false;

    bool hasReleasedJumpButton = true;

    void FixedUpdate()
    {
        //isGrounded = Physics2D.OverlapCircle(groundCheck.position, 0.05f, groundLayer);

        isGrounded = Physics2D.OverlapArea(topLeftGroundCheck.position, bottomRightGroundCheck.position);
        //print(isGrounded);
    }

    // Update is called once per frame
    void Update () {

        //Walking
        float moveX = Input.GetAxisRaw("Horizontal");

        Vector2 movement = new Vector2(moveX, 0) * Time.deltaTime * speed;

        transform.Translate(movement);

        //Jumping
        float jump = Input.GetAxisRaw("Jump");

        if (jump > 0 && hasReleasedJumpButton == true && isGrounded)
        {
            Rigidbody2D rb = GetComponent<Rigidbody2D>();

            Vector3 jumpVector = Vector3.up * jumpForce;

            rb.AddForce(jumpVector);

            print("JUMP");
            hasReleasedJumpButton = false;
        }
        else if (!(jump > 0))
        {
            hasReleasedJumpButton = true;
        }

        
	}
}
