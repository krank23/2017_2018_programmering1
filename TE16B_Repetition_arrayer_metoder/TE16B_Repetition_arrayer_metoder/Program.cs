﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE16B_Repetition_arrayer_metoder
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] name = { "Edwin", "Klara", "Kia" };

            int[] height = new int[3];

            int[] pos = { 2, 1 };

            /*int i = 2;
             * 

            name[i] = "Kalle";*/

            //Console.WriteLine(name[pos[0]]);

            List<string> longNames = new List<string>();

            longNames.Add("Pelle");
            longNames[0] = "Kalle";
            longNames.RemoveAt(0);

            for (int i = 0; i < name.Length; i++)
            {
                if (name[i].Length > 4)
                {
                    //Console.WriteLine(name[i]);
                    longNames.Add(name[i]);

                    //Hello(name[i]);
                }
            }

            Hellos(name);

            //Console.WriteLine(name[0]);


            Hello("Micke");
            
            Console.ReadLine();
        }

        static void Hello(string n)
        {
            Console.WriteLine(n + " SAYS HELLO");
        }

        static void Hellos(string[] ns)
        {
            for (int i = 0; i < ns.Length; i++)
            {
                Hello(ns[i]);
            }
        }

        static void Goodbye()
        {
            Console.WriteLine("YOU SAY GOODBYE");
        }

    }
}
