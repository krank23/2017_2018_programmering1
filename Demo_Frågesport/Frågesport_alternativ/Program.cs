﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frågesport_alternativ
{
    class Program
    {
        static void Main(string[] args)
        {
            // Här deklarerar jag mina variabler
            int points = 0;
            string a;


            Console.WriteLine("All your base are belong to...");
            Console.WriteLine("1: everyone  2: the empire  3: us  4: me");
            a = Console.ReadLine();

            if (a == "3")
            {
                Console.WriteLine("Rätt!");
                points++;
            }
            else
            {
                Console.WriteLine("Fel!");
            }

            Console.WriteLine("Hur många robotbossar behövde Megaman slåss mot i det första Megamanspelet?");
            Console.WriteLine("1: 8  2: 4  3: 12  4: 6");
            a = Console.ReadLine();

            if (a == "4")
            {
                Console.WriteLine("Rätt!");
                points++;
            }
            else
            {
                Console.WriteLine("Fel!");
            }

            Console.WriteLine("Vad heter huvudfienderna i Mass Effect 1-3?");
            Console.WriteLine("1: Geth  2: Reapers  3: Salarians  4: Humans");
            a = Console.ReadLine();

            if (a == "2")
            {
                Console.WriteLine("Rätt!");
                points++;
            }
            else
            {
                Console.WriteLine("Fel!");
            }

            Console.WriteLine("\nDu fick " + points + " poäng av 3 möjliga.");

            Console.ReadLine();
        }
    }
}
