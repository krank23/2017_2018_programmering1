﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE16A_Frågesport
{
    class Program
    {
        static void Main(string[] args)
        {

            int points = 0;

            Console.WriteLine("Vad blir 6 + 66?");
            Console.WriteLine("a) 45 b) 667 c) 72 d) 666");

            string answer = "";
            while (answer != "a" && answer != "b" && answer != "c" && answer != "d")
            {
                answer = Console.ReadLine();
                answer = answer.ToLower();
            }

            if (answer == "c" || answer == "d")
            {
                Console.WriteLine("RÄTT!");
                points+=500;
            }
            else
            {
                Console.WriteLine("FEL!");
            }

            Console.ReadLine();
        }
    }
}
