﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE16A_String_manipulation
{
    class Program
    {
        static void Main(string[] args)
        {

            //char c = 'r';

            string name = "Micke";

            //name = name.ToLower();

            //Console.WriteLine(name.ToUpper());

            /*if (name.ToLower() == "micke")
            {

            }

            Console.WriteLine(name);*/

            /*string welcome = "  hej  \n  ";

            Console.WriteLine(welcome.Trim() + name);

            Console.ReadLine();*/

            string sentence = "Lorem ipsum dolor sit amet";

            string[] words = sentence.Split(' ');

            Console.WriteLine(words[0]);

            // Gorgon;200;20;5



            words = words.Reverse().ToArray();

            sentence = String.Join(" ", words);

            Console.WriteLine(sentence);

            string[] names = { "Micke", "Martin", "Lena" };

            Console.WriteLine(String.Join(" är dum och ", names).Replace("dum", "smart"));

            Console.WriteLine(sentence.IndexOf("ips"));

            Console.WriteLine(sentence.Substring(4,6));

            /* ToUpper, ToLower
             * Trim
             * Split
             * Join
             * IndexOf
             * Substring
             * Replace
             */


            Console.ReadLine();
        }
    }
}
